package com.atlassian.maven.enforcer;

import org.apache.maven.enforcer.rule.api.EnforcerRule;
import org.apache.maven.enforcer.rule.api.EnforcerRuleException;
import org.apache.maven.enforcer.rule.api.EnforcerRuleHelper;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.PluginExecution;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.component.repository.exception.ComponentLookupException;
import org.codehaus.plexus.util.StringUtils;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.List;
import java.util.Optional;


/**
 * Check Maven plugin configurations and verify they have required configuration items.
 *
 * <pre>
 * &lt;ensurePluginConfig implementation="com.atlassian.maven.enforcer.EnsurePluginConfigurationRule"&gt;
 *     &lt;plugin&gt;
 *         &lt;groupId&gt;com.example.plugin&lt;/groupId&gt;
 *         &lt;artifactId&gt;example-maven-plugin&lt;/artifactId&gt;
 *     &lt;/plugin&gt;
 *     &lt;configurations&gt;
 *         &lt;configuration&gt;
 *             &lt;xpath&gt;//system/property&lt;/xpath&gt;
 *             &lt;regexp&gt;java-.+&lt;/regexp&gt;
 *             &lt;message&gt;Make sure a `java-` System property is set.&lt;/message&gt;
 *         &lt;/configuration&gt;
 *     &lt;/configurations&gt;
 * &lt;/ensurePluginConfig&gt;
 * </pre>
 */
public class EnsurePluginConfigurationRule implements EnforcerRule {

    /**
     * Plugins to match.
     */
    private PluginFilter plugin;

    /**
     * Configuration rules to enforce.
     */
    private List<Configuration> configurations;

    @Override
    public void execute(EnforcerRuleHelper helper) throws EnforcerRuleException {
        checkPreconditions();

        try {
            MavenProject project = helper.getComponent(MavenProject.class);

            for (Plugin plugin : project.getBuildPlugins()) {
                if (!matchesPlugin(plugin)) {
                    continue;
                }

                boolean hasValidConfig = false;

                try {
                    validate((Xpp3Dom) plugin.getConfiguration());

                    hasValidConfig = plugin.getConfiguration() != null;
                } catch (PluginConfigCheckFailed failure) {
                    // if there are plugins, give them a chance, otherwise fail
                    if (plugin.getExecutions().isEmpty()) {
                        fail(plugin, null, failure.getMessage());
                    }
                }

                for (PluginExecution execution : plugin.getExecutions()) {
                    try {
                        validate(((Xpp3Dom) execution.getConfiguration()));
                    } catch (PluginConfigCheckFailed failure) {
                        // if the plugin config was valid, we're OK, otherwise fail
                        if (!hasValidConfig) {
                            fail(plugin, execution, failure.getMessage());
                        }
                    }
                }
            }
        } catch (ComponentLookupException | XPathExpressionException e) {
            throw new EnforcerRuleException("Failed to execute plugin configuration checks", e);
        }
    }

    /**
     * Make sure our enforcer configuration is valid.
     *
     * @throws EnforcerRuleException
     */
    private void checkPreconditions() throws EnforcerRuleException {
        Optional.ofNullable(plugin).orElseThrow(() -> new EnforcerRuleException("Missing <plugin> filter"));
        Optional.ofNullable(plugin.getArtifactId())
                .filter(StringUtils::isNotEmpty)
                .orElseThrow(() -> new EnforcerRuleException("Missing <artifactId> in the plugin filter"));

        if (configurations.isEmpty()) {
            throw new EnforcerRuleException("No <configurations> rules are given");
        }

        for (Configuration config : configurations) {
            Optional.ofNullable(config.getXpath())
                    .filter(StringUtils::isNotEmpty)
                    .orElseThrow(() -> new EnforcerRuleException("Missing <xpath> in the rule configuration"));

            Optional.ofNullable(config.getRegexp())
                    .filter(StringUtils::isNotEmpty)
                    .orElseThrow(() -> new EnforcerRuleException("Missing <regexp> in the rule configuration"));
        }
    }

    /**
     * Check if the plugin has the same artifact ID (mandatory) and the same group ID (optional).
     *
     * @param testPlugin the plugin to check
     * @return true if this is a plugin to enforce configuration rules on
     */
    private boolean matchesPlugin(Plugin testPlugin) {
        if (!testPlugin.getArtifactId().equals(plugin.getArtifactId())) {
            return false;
        }

        if (plugin.getGroupId() != null) {
            return testPlugin.getGroupId().equals(plugin.getGroupId());
        } else {
            return true;
        }
    }

    /**
     * Validates the plugin or execution configuration against the given rules.
     * Missing configuration is valid.
     *
     * @param dom the configuration as XML dom or null
     * @throws PluginConfigCheckFailed  if the configuration doesn't match the rules
     * @throws XPathExpressionException if a configuration rule's XPath is invalid
     */
    private void validate(Xpp3Dom dom) throws PluginConfigCheckFailed, XPathExpressionException {
        if (dom == null) {
            return;
        }

        for (Configuration config : configurations) {
            XPath xPath = XPathFactory.newInstance().newXPath();
            String content = xPath.evaluate(config.getXpath(), new InputSource(new StringReader(dom.toString())));

            if (!content.matches(config.getRegexp())) {
                throw new PluginConfigCheckFailed(config.getMessage());
            }
        }
    }

    /**
     * Construct and throw an enforcer rule failure.
     *
     * @param plugin    the plugin that had invalid configuration
     * @param execution the plugin execution that had invalid configuration, if applicable, otherwise null
     * @param message   the failure message or null
     * @throws EnforcerRuleException
     */
    private void fail(Plugin plugin, PluginExecution execution, String message) throws EnforcerRuleException {
        String coordinates = String.format("%s:%s", plugin.getGroupId(), plugin.getArtifactId());
        if (execution != null) {
            coordinates = String.format("%s:%s", coordinates, execution.getId());
        }

        String failMessage = String.format("Plugin configuration failed for %s", coordinates);
        if (StringUtils.isNotEmpty(message)) {
            failMessage = String.format("%s: %s", failMessage, message);
        }

        throw new EnforcerRuleException(failMessage);
    }

    @Override
    public boolean isCacheable() {
        return false;
    }

    @Override
    public boolean isResultValid(EnforcerRule enforcerRule) {
        return false;
    }

    @Override
    public String getCacheId() {
        return "EnsurePluginConfigurationRule";
    }

    public static class PluginFilter {

        String groupId;
        String artifactId;

        String getGroupId() {
            return groupId;
        }

        String getArtifactId() {
            return artifactId;
        }
    }

    public static class Configuration {

        String xpath;
        String regexp;
        String message;

        String getXpath() {
            return xpath;
        }

        String getRegexp() {
            return regexp;
        }

        String getMessage() {
            return message;
        }
    }

    static class PluginConfigCheckFailed extends Exception {
        PluginConfigCheckFailed(String message) {
            super(message);
        }
    }

}
