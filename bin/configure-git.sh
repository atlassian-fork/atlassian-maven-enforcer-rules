#!/bin/sh

#
# For deployment-bamboo, the git user must be the Build Agent
#

git config --global user.email "build-agent@atlassian.com"
git config --global user.name "Build Agent"
