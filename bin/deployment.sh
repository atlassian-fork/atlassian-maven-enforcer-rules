#!/bin/sh

set -ex

# Set user name and email
./bin/configure-git.sh

# Release it
./bin/maven-release.sh
