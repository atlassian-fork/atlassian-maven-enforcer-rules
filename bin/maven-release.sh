#!/bin/sh

set -ex

# Use the -Psox profile to get the artifacts GPG signed on deployment-bamboo
# Prepare and perform the maven release
mvn --batch-mode --errors release:prepare -Darguments="--activate-profiles sox" -DscmCommentPrefix="[skip ci] " -DautoVersionSubmodules=true

# Release the artifact to artifactory
mvn --batch-mode --errors release:perform -DlocalCheckout=true
